import React from "react";
import { NextPage, GetStaticProps } from "next";
import { Event } from "../models/events";
import useFetchEvents, { EVENT_URL } from "../hooks/useFetchEvents";
import UpcomingEvents from "../components/UpcomingEvents";
import ErrorView from "../views/ErrorView";
import { fetcher } from "../utils";

const EVENT_LIMIT = 3;

interface InfoscreenInitialProps {
  initialEvents: Event[];
}

const InfoscreenPage: NextPage<InfoscreenInitialProps> = ({ initialEvents }) => {
  const { events, error } = useFetchEvents(initialEvents);
  if (error) {
    return (
      <ErrorView statusCode={500} />
    );
  }
  return (
    <UpcomingEvents events={events ?? []} />
  );
};

export const getStaticProps: GetStaticProps<InfoscreenInitialProps> = async () => {
  const initialEvents = await fetcher<Event[]>(EVENT_URL);
  return {
    props: {
      initialEvents: initialEvents.slice(0, EVENT_LIMIT),
    },
    revalidate: 60,
  };
};

export default InfoscreenPage;
