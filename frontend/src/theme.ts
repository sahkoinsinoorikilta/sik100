const fontSizes = {
  smaller: "10px",
  small: "12px",
  normal: "16px",
  h2: "24px",
  bigger: "36px",
  biggest: "48px",
};

const spacing = {
  xxs: "4px",
  xs: "8px",
  sm: "16px",
  md: "24px",
  lg: "48px",
  xlg: "96px",
};

const breakpoints = {
  xxxs: "375px",
  xxs: "480px",
  xs: "600px",
  sm: "840px",
  md: "1080px",
  lg: "1440px",
  xlg: "1920px",
};

const colors = {
  darkBlue1: "#130D3F",
  darkBlue2: "#12123D",
  blue3: "#282561",
  blue4: "#19235D",
  blue5: "#252160",
  blue6: "#191849",
  primaryGold: "#FFD581",
  secondaryGold: "#FDF4A6",
  goldGradient: "linear-gradient(90deg, rgba(255,213,129,1) 0%, rgba(253,244,166,1) 31%, rgba(255,247,195,1) 62%, rgba(255,213,129,1) 100%)",
  yellow: "#FEE469",
  sand: "#FEF9D9",
  orange1: "#F3783A",
  orange2: "#F49b61",
  // turquoise: "#BEDDEB",
  white: "#FFF",
  black: "#000",
  // Guild colors
  // darkBlue: "#002D3A",
  // lightBlue: "#BFDBD9",
  // grey1: "#D4D0C7",
  // grey2: "#EFECE4",
  grey1: "#b2b2b2",
  // orange1: "#D57A2D",
  // orange2: "#DD934E",
  // blue: "#57B2DF",
  turquoise: "#6AC7C4",
  // green: "#C0DCD9",
  // sand: "#FDF9D7",
};

export type ThemeType = {
  maxContentWidth: "1000px";
  colors: typeof colors;
  spacing: typeof spacing;
  breakpoints: typeof breakpoints;
  fontSizes: typeof fontSizes;
};

const theme: ThemeType = {
  maxContentWidth: "1000px",
  colors,
  spacing,
  breakpoints,
  fontSizes,
};

export default theme;
