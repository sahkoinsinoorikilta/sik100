module.exports = {
  siteUrl: process.env.NEXT_PUBLIC_SITE_URL || "https://sik100.fi",
  generateRobotsTxt: true,
  exclude: [],
};
