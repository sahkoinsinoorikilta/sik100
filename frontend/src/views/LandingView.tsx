import React from "react";
import styled from "styled-components";
import Link from "next/link";
import { useTranslation } from "../i18n";
import { Event } from "../models/events";
import { InstagramObject } from "../models/instagram";
import { SossoPost } from "../models/sosso";
import Header from "../components/Header";
import Footer from "../components/Footer";
import InstagramFeed from "../components/InstgaramFeed";
import GoldHR from "../components/GoldHR";
import Sponsors from "../components/Sponsors";
import UpcomingEvents from "../components/UpcomingEvents";
import Quote from "../components/Quote";
import theme from "../theme";
import HistoryArticle from "../components/HistoryArticle";

const EVENT_LIMIT = 3;

const QuoteContainer = styled.aside`
  flex: 2;
  justify-self: flex-end;

  @media screen and (max-width: ${theme.breakpoints.sm}) {
    display: none;
  }
`;

const HistorySection = styled.section`
  width: 100%;
  max-width: unset;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  background-color: ${theme.colors.turquoise};
  background-image: url('/images/timanttielementti.png');
  background-position: right;
  background-repeat: no-repeat;
  background-size: ${theme.breakpoints.md};

  @media screen and (max-width: ${theme.breakpoints.xs}) {
    background-size: cover;
  }
`;

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const HistoryCTA = styled.a`
  text-decoration: none;
  width: 100%;
  text-align: center;
  font-size: ${theme.fontSizes.h2};
  background-color: ${theme.colors.orange1};
  color: ${theme.colors.white};
  padding: ${theme.spacing.md};

  &:focus,
  &:visited,
  &:link,
  &:active {
    text-decoration: none;
  }

  &:hover {
    color: ${theme.colors.primaryGold} !important;
    transition: color 0.5s ease-out;
  }
`;

const TextSection = styled.section`
  display: flex;

  & > article {
    flex: 3;
  }

  a {
    text-transform: uppercase;
    text-decoration: none;
    font-weight: bold;
    color: ${theme.colors.white};
    letter-spacing: 1px;

    &:focus,
    &:hover,
    &:visited,
    &:link,
    &:active {
      text-decoration: none;
    }

    &:hover {
      color: ${theme.colors.primaryGold};
      transition: color 0.5s ease-out;
    }
  }
`;

const Arrow = styled.div`
  display: inline-block;
  margin-left: 12px;
  width: 0;
  height: 0;
  border-top: 6px solid transparent;
  border-bottom: 6px solid transparent;
  border-left: 6px solid ${theme.colors.primaryGold};
`;

const EventSection = styled.section`
  width: 100%;
  padding: 0 ${theme.spacing.md};
  margin-bottom: ${theme.spacing.lg};

  a {
    display: inline-block;
    text-transform: uppercase;
    text-decoration: none;
    font-weight: bold;
    color: ${theme.colors.white};
    letter-spacing: 1px;

    &:focus,
    &:hover,
    &:visited,
    &:link,
    &:active {
      text-decoration: none;
    }

    &:hover {
      color: ${theme.colors.primaryGold};
      transition: color 0.5s ease-out;
    }
  }

  h2 {
    text-transform: uppercase;
    color: ${theme.colors.white};
    font-size: ${theme.fontSizes.h2};
    text-align: center;
    margin: ${theme.spacing.lg} 0;
  }
`;

const InstagramSection = styled.section`
  width: 100%;
  background-color: ${theme.colors.blue6};
  max-width: unset;
  display: flex;
  flex-direction: column;
  align-items: center;

  & > * {
    max-width: ${theme.maxContentWidth};
  }
`;

const SponsorSection = styled.section`
  padding: 0 ${theme.spacing.md} ${theme.spacing.lg};
  width: 100%;
  background-color: ${theme.colors.blue4};
  max-width: unset;
  display: flex;
  flex-direction: column;
  align-items: center;

  & > * {
    max-width: ${theme.maxContentWidth};
  }
`;

interface LandingViewProps {
  events: Event[];
  instagramFeed: InstagramObject[];
  sossoPost?: SossoPost;
}

const LandingView: React.FC<LandingViewProps> = ({ events, instagramFeed, sossoPost }) => {
  const { t } = useTranslation();
  return (
    <>
      <Header>
        <p>
          {t("frontBanner1")}
        </p>
        <p>
          {t("frontBanner2")}
          {" "}
          <strong>{t("frontBanner3")}</strong>
          {" "}
          {t("frontBanner4")}
        </p>
      </Header>
      <main>
        <TextSection>
          <article>
            <h2>
              {t("events")}
            </h2>
            <p>
              {t("frontP")}
            </p>
          </article>

          <QuoteContainer>
            <Quote />
          </QuoteContainer>
        </TextSection>

        <EventSection>
          <h2>
            {t("upcomingHeader")}
          </h2>
          <UpcomingEvents events={events.slice(0, EVENT_LIMIT)} />
          <Link href="/events">
            <a href="/events">
              {t("Katso kaikki tapahtumat")}
              <Arrow />
            </a>
          </Link>
        </EventSection>

        <GoldHR />

        <HistorySection>
          {sossoPost && (
            <>
              <GoldHR />
              <HistoryArticle
                title={sossoPost.title}
                excerpt={sossoPost.excerpt}
                link={sossoPost.url}
                image={sossoPost.image}
              />
            </>
          )}
          {/* <HistoryCTA href="https://sosso.fi/tag/sik100/">
            {t("Lue SIK100 aiheisia artikkeleja Sössöstä!")}
          </HistoryCTA> */}
        </HistorySection>

        <SponsorSection>
          <Sponsors />
        </SponsorSection>

        <InstagramSection>
          <InstagramFeed feed={instagramFeed} />
        </InstagramSection>
      </main>
      <Footer />
    </>
  );
};

export default LandingView;
