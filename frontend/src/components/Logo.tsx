import React from "react";
import Image from "next/image";

const Logo: React.FC = () => (
  <Image
    src="/images/SIK_W.svg"
    alt="SIK"
    width={120}
    height={100}
    loading="lazy"
  />
);

export default Logo;
