import React from "react";
import { NextPage, NextPageContext } from "next";
import { useTranslation } from "../i18n";
import ErrorView from "../views/ErrorView";

interface ServerErrorPageProps {
  statusCode: number;
}

const ServerErrorPage: NextPage<ServerErrorPageProps> = ({ statusCode }) => {
  const { t } = useTranslation();
  return (
    <ErrorView statusCode={statusCode}>
      <h3>{t("Hups, tapahtui virhe")}</h3>
    </ErrorView>
  );
};

// any modifications to the default context, e.g. query types
// interface Context extends NextPageContext {}

export const getInitialProps = (ctx: NextPageContext) => {
  const statusCode = ctx.res?.statusCode || ctx.err?.statusCode || 404;
  return { statusCode };
};

export default ServerErrorPage;
