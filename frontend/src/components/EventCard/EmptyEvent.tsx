import styled from "styled-components";
import theme from "../../theme";

const EmptyEvent = styled.article`
  display: flex;
  min-height: 103px;

  @media screen and (max-width: ${theme.breakpoints.xs}) {
    min-height: 90px;
  }

  @media screen and (max-width: ${theme.breakpoints.xxs}) {
    min-height: 80px;
  }

  @media screen and (max-width: ${theme.breakpoints.xxxs}) {
    min-height: 72px;
  }

  width: 100%;
  margin: ${theme.spacing.sm} 0;
  background: linear-gradient(to right, #000, #fff, #000);
  background-size: 200% 100%;
  animation: AnimationName 3s linear infinite;

  @keyframes AnimationName {
    0% { background-position: 0% 0%; }
    50% { background-position: 100% 0%; }
    100% { background-position: 200% 0%; }
  }
`;

export default EmptyEvent;
