import React, { ReactNode } from "react";
import Image from "next/image";
import styled from "styled-components";
import theme from "../theme";

const Container = styled.div`
  position: relative;
  background: url(/images/banner_bg.png);
  margin-bottom: ${theme.spacing.lg};
`;

const HeaderContainer = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
`;

const LogoContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  margin: auto;
  line-height: 1.3;
  padding: ${theme.spacing.lg} ${theme.spacing.xlg} ${theme.spacing.xlg};
  max-width: 1000px;

  @media screen and (max-width: ${theme.breakpoints.sm}) {
    padding: ${theme.spacing.sm} ${theme.spacing.sm} ${theme.spacing.xlg};
    flex-direction: column;
  }
`;

const BannerAction = styled.div`
  flex: 1 0;
  margin-left: ${theme.spacing.lg};
  font-size: 18px;
  color: white;

  @media screen and (max-width: ${theme.breakpoints.sm}) {
    margin-left: 0;
  }
`;

const Header = styled.h1`
  background-color: ${theme.colors.turquoise};
  position: absolute;
  bottom: -15%;
  text-align: center;
  text-transform: uppercase;
  font-weight: bold;
  padding: ${theme.spacing.xs} ${theme.spacing.xlg};

  @media screen and (max-width: ${theme.breakpoints.xxs}) {
    padding: ${theme.spacing.sm} 0;
    width: 100%;
  }
`;

export type BannerProps = {
  title?: ReactNode;
  golden?: boolean;
};

const Banner: React.FC<BannerProps> = ({ title, golden, children }) => {
  const style = golden ? {
    background: theme.colors.goldGradient,
  } : undefined;
  return (
    <Container>
      <LogoContainer>
        <Image
          src="/images/100_logo.svg"
          alt="SIK100 logo"
          width={256}
          height={150}
          loading="lazy"
        />
        { children && (
        <BannerAction>
          {children}
        </BannerAction>
        )}
      </LogoContainer>
      { title && (
      <HeaderContainer>
        <Header
          style={style}
        >
          {title}
        </Header>
      </HeaderContainer>
      )}
    </Container>
  );
};

export default Banner;
