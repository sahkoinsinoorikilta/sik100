import React from 'react';
import Axios from "axios";
import { toast } from "react-toastify";
import styled from 'styled-components';
import { useTranslation } from "../i18n";
import { SIGNUP_FORM_URL } from "../hooks/useFetchSignup";
import { EventSignupForm } from "../models/events";
import Footer from "../components/Footer";
import Header from "../components/Header";
import SignupForm from "../components/SignupForm/SignupForm";
import theme from "../theme";
import config from "../config";
import { mutate } from 'swr';

interface SignupViewProps {
  signupForm: EventSignupForm;
}

const SignupSection = styled.article`
  padding: ${theme.spacing.md};
  display: grid;
  grid-template-columns: 1fr 2fr 1fr;
  grid-template-rows: 1fr auto;
  grid-template-areas:
    ". title ."
    "leftaside content rightaside";

  @media screen and (max-width: ${theme.breakpoints.md}) {
    grid-template-columns: 1fr;
    grid-template-rows: 1fr auto auto auto;
    grid-template-areas:
      "title"
      "content"
      "rightaside"
      "leftaside";
  }

  & > div,
  p {
    grid-area: content;
  }

  & > aside {
    grid-area: rightaside;
    padding-left: 1.5rem;
  }
`;

const FormContainer = styled.div`
  color: ${theme.colors.white};
  background-color: ${theme.colors.darkBlue2};
  padding: ${(p) => `${p.theme.spacing.lg} ${p.theme.spacing.xlg}`};

  @media screen and (max-width: ${theme.breakpoints.md}) {
    padding: ${(p) => `${p.theme.spacing.lg}`};
  }

  @media screen and (max-width: ${theme.breakpoints.sm}) {
    padding: ${(p) => `${p.theme.spacing.md}`};
  }

  @media screen and (max-width: ${theme.breakpoints.xxs}) {
    padding: ${(p) => `${p.theme.spacing.sm} ${p.theme.spacing.md}`};
  }

  button {
    margin-top: ${theme.spacing.sm};
    margin-bottom: ${theme.spacing.sm};
  }

  & > h3 {
    color: ${theme.colors.secondaryGold};
  }

  a {
    font-weight: bold;
    color: ${theme.colors.secondaryGold};
    letter-spacing: 1px;
    text-decoration: underline;

    &:hover {
      color: ${theme.colors.primaryGold};
    }
  }
`;

const StyledAside = styled.aside`
  color: ${theme.colors.white};

  li {
    padding-bottom: 0.3rem;
    font-weight: bold;
  }

  .reserved {
    color: ${theme.colors.grey1};
  }
`

// eslint-disable-next-line camelcase
const sendSignup = async (signupForm_id: number, answer: string) => {
  const url = `${config.backendUrl}/signup`;
  const result = await Axios.post(url, {
    signupForm_id,
    answer,
  });
  return result.data.data;
};

const SignupView: React.FC<SignupViewProps> = ({signupForm}) => {
  const { t } = useTranslation();

  const renderList = () => (
      <div>
        <h4>
          {t("Ilmoittautuneet")} {signupForm.quota > 0 && (` (${signupForm.signups.length}/${signupForm.quota})`)}:
        </h4>
        <ol data-e2e="signup-list">
          {signupForm.signups.map((s, idx) => (
            <li key={idx} className={signupForm.quota && idx + 1 > signupForm.quota ? "reserved" : ""}>{s}</li>
          ))}
        </ol>
      </div>
  );

  return (
    <>
      <Header />
      <main>
        <SignupSection>
          <FormContainer>
            <SignupForm
              signup={signupForm}
              onSubmit={async (data) => {
                try {
                  await sendSignup(signupForm.id, data.formData);
                  window.scrollTo({left: 0, top: 0, behavior: "smooth"});
                  toast.success("Sign-up submitted successfully", {
                    position: "bottom-right",
                    autoClose: 10000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                  });
                  mutate(`${SIGNUP_FORM_URL}${signupForm.id}/`);
                } catch (error) {
                  // eslint-disable-next-line no-console
                  console.error(error);
                  toast.error("Bad request", {
                    position: "bottom-right",
                    autoClose: 10000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                  });
                }
              }}
            />
          </FormContainer>
          <StyledAside>
            {renderList()}
          </StyledAside>
        </SignupSection>
      </main>
      <Footer />
    </>
  );
}

export default SignupView;