import React from "react";
import Image from "next/image";
import styled from "styled-components";
import { useTranslation } from "../i18n";
import useWindowWidth from "../hooks/useWindowWidth";
import theme from "../theme";
import MinorSponsorMarquee from "./MinorSponsorMarquee";

const imagePath = "/images/logos/";
const format = "_logo_white.png";

// Main sponsor logos
const ABBLogo = `${imagePath}ABB${format}`;
const TTERLogo = `${imagePath}TTER${format}`;

// Minor sponsor logos
const eSettLogo = `${imagePath}eSett${format}`;
const carunaLogo = `${imagePath}Caruna${format}`;
const fingridLogo = `${imagePath}Fingrid${format}`;
const fennoVoimaLogo = `${imagePath}FennoVoima${format}`;
const okmeticLogo = `${imagePath}Okmetic${format}`;
const enstoLogo = `${imagePath}Ensto${format}`;
const helenLogo = `${imagePath}Helen${format}`;
const siemensLogo = `${imagePath}Siemens${format}`;
const eatonLogo = `${imagePath}Eaton${format}`;
const tekLogo = `${imagePath}TEK${format}`;
const lumineqLogo = `${imagePath}Lumineq${format}`;
const beneqLogo = `${imagePath}Beneq${format}`;
const swecoLogo = `${imagePath}Sweco${format}`;
const helmetLogo = `${imagePath}Helmet${format}`;
const audiLogo = `${imagePath}Audi${format}`;
const profitLogo = `${imagePath}Profit${format}`;
const afryLogo = `${imagePath}Afry${format}`;

// const eSettLogo = "https://static.sika.sik.party/sik100/sponsors/eSett_logo.png";

const LARGE_IMG_SIZE = 300;
const SMALL_IMG_SIZE = 200;

const MainSponsors = styled.div`
  display: flex;
  flex-flow: row wrap;
  justify-content: space-around;
  align-items: center;
  margin: ${theme.spacing.md};
  width: 100%;

  & > * {
    margin: 0 ${(p) => p.theme.spacing.lg} !important;
  }

  @media screen and (max-width: ${theme.breakpoints.xxs}) {
    margin: ${theme.spacing.xs};
  }
`;

const SponsorTitle = styled.h2`
  color: ${theme.colors.yellow};
  text-transform: uppercase;
  text-align: center;
  padding: ${theme.spacing.sm};
`;

const Sponsors: React.FC = () => {
  const { width } = useWindowWidth();
  const { t } = useTranslation();
  let speed = 40;
  if (width && width < Number(theme.breakpoints.sm.slice(0, -2))) {
    speed = 20;
  }

  return (
    <>
      <SponsorTitle>{t("sponsored by")}</SponsorTitle>
      <MainSponsors>
        <Image
          src={ABBLogo}
          alt="ABB"
          width={LARGE_IMG_SIZE}
          height={LARGE_IMG_SIZE}
          objectFit="scale-down"
        />
        <Image
          src={TTERLogo}
          alt="TTER"
          width={LARGE_IMG_SIZE}
          height={LARGE_IMG_SIZE}
          objectFit="scale-down"
        />
      </MainSponsors>
      <MinorSponsorMarquee
        speed={speed}
        imgSize={SMALL_IMG_SIZE}
        imgs={[
          {
            alt: "eSett",
            src: eSettLogo,
          },
          {
            alt: "Caruna",
            src: carunaLogo,
          },
          {
            alt: "Fingrid",
            src: fingridLogo,
          },
          {
            alt: "FennoVoima",
            src: fennoVoimaLogo,
          },
          {
            alt: "Okmetic",
            src: okmeticLogo,
          },
          {
            alt: "Ensto",
            src: enstoLogo,
          },
          {
            alt: "Helen",
            src: helenLogo,
          },
          {
            alt: "Siemens",
            src: siemensLogo,
          },
          {
            alt: "TEK",
            src: tekLogo,
          },
          {
            alt: "Eaton",
            src: eatonLogo,
          },
          {
            alt: "Lumineq",
            src: lumineqLogo,
          },
          {
            alt: "Beneq",
            src: beneqLogo,
          },
          {
            alt: "Sweco",
            src: swecoLogo,
          },
          {
            alt: "Helmet",
            src: helmetLogo,
          },
          {
            alt: "Audi",
            src: audiLogo,
          },
          {
            alt: "Profit",
            src: profitLogo,
          },
          {
            alt: "Afry",
            src: afryLogo,
          },
        ]}
      />
    </>
  );
};

export default Sponsors;
