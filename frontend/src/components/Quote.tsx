import React from "react";
import styled from "styled-components";
import { useTranslation } from "../i18n";

const Container = styled.div`
  background: rgba(0, 0, 0, 0.2);
  margin-left: 48px;
  border-radius: 12px;
  padding: 12px 24px;
`;

const Text = styled.p`
  font-size: 18px;
  font-style: italic;
`;

const Author = styled.p`
  font-weight: bold;
  text-align: right;
`;

const Quote: React.FC = () => {
  const { t } = useTranslation();
  return (
    <Container>
      <Text>
        {t("quote")}
      </Text>
      <Author>
        —
        {" "}
        {t("Eitel Timgren")}
      </Author>
    </Container>
  );
};

export default Quote;
