import React from "react";
import Image from "next/image";
import styled from "styled-components";
import theme from "../theme";

const Article = styled.article`
  color: ${theme.colors.darkBlue2};
  padding: ${theme.spacing.sm};
  display: flex;
  flex-direction: row;
  justify-content: center;
  margin: 0 auto;

  @media screen and (max-width: ${theme.breakpoints.xs}) {
    flex-direction: column;
  }

  & > div {
    display: flex;
    flex-direction: column;
    flex: 1;
    max-width: 600px;
    align-items: center;
    padding: ${theme.spacing.sm};
    margin: 0 50px;

    @media screen and (max-width: ${theme.breakpoints.md}) {
      margin: 0 auto;
    }
  }

  h2 {
    text-transform: uppercase;
    color: ${theme.colors.black};
    font-size: ${theme.fontSizes.h2};
    text-align: center;
  }

  p {
    font-weight: 600;
  }

  a {
    display: block;
    text-decoration: none;
    text-transform: uppercase;
    color: ${theme.colors.darkBlue2};

    &:focus,
    &:visited,
    &:link,
    &:active {
      text-decoration: none;
    }

    &:hover {
      color: ${theme.colors.primaryGold};
      transition: color 0.5s ease-out;
    }
  }
`;

interface HistoryArticleProps {
  title: string;
  excerpt: string;
  link: string;
  image: string;
}

const HistoryArticle: React.FC<HistoryArticleProps> = ({
  title, excerpt, link, image,
}) => (
  <Article>
    <div>
      <a href={link}>
        <Image
          src={image}
          alt={title}
          width={600}
          height={400}
          layout="intrinsic"
        />
      </a>
    </div>
    <div>
      <h2>{title}</h2>
      {/* eslint-disable-next-line react/no-danger */}
      <div dangerouslySetInnerHTML={{ __html: excerpt }} />
    </div>
  </Article>
);

export default HistoryArticle;
