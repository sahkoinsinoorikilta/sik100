import useSWR from "swr";
import config from "../config";
import { EventSignupForm } from "../models/events";
import { fetcher } from "../utils";

export const SIGNUP_FORM_URL = `${config.backendUrl}/signupForm/`;

const useFetchSignup = (initialData?: EventSignupForm, id?: String) => {
  const url = `${SIGNUP_FORM_URL}${id}/`
  const { data, error } = useSWR<EventSignupForm | undefined>(url, fetcher, { initialData, revalidateOnFocus: false });
  return {
    signupForm: data,
    error,
  };
};

export default useFetchSignup;