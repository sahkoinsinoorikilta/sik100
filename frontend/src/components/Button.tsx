import React, { ButtonHTMLAttributes } from "react";
import styled from "styled-components";
import theme from "../theme";

export type ButtonProps = ButtonHTMLAttributes<HTMLButtonElement> & {
};

const GradientBorder = styled.button`
  background: ${theme.colors.goldGradient};
  padding: 1px;
  cursor: pointer;

  & > div {
    background: ${theme.colors.darkBlue2};
    padding: ${(p) => `${p.theme.spacing.sm} ${p.theme.spacing.md}`};
    text-transform: uppercase;
    text-align: center;
    color: ${theme.colors.white};

    @media screen and (max-width: ${theme.breakpoints.sm}) {
      font-size: ${theme.fontSizes.smaller};
      padding: ${(p) => `${p.theme.spacing.xs} ${p.theme.spacing.sm}`};
    }
  }
`;

const Button: React.FC<ButtonProps> = ({ children, ...props }) => (
  <GradientBorder {...props}>
    <div>
      {children}
    </div>
  </GradientBorder>
);

export default Button;
