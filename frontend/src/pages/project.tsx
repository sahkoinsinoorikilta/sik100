import React from "react";
import { NextPage, GetStaticProps } from "next";
import ProjectView from "../views/ProjectView";

const ProjectsPage: NextPage = () => (
  <ProjectView />
);

export const getStaticProps: GetStaticProps = async () => ({
  props: {},
});

export default ProjectsPage;
