/* eslint-disable no-console */
import express from "express";
import axios, { AxiosResponse } from "axios";
import environment from "../environment";
import { EventResponse, SignupFormResponse, SignupForm } from "./eventSchema";
import { InstagramResponse } from "./instagramSchema";
import { SossoResponse } from "./sossoSchema";

const router = express.Router();

/* GET home page. */
router.get("/", (req, res) => res.status(200).json({ msg: "SIK100 API" }));

router.get("/events", async (req, res) => {
  const url = `${environment.EVENT_BACKEND}/events/?tags=${environment.EVENT_TAG_ID}`;
  try {
    const resp: AxiosResponse<EventResponse> = await axios.get(url);
    const { data: { results } } = resp;
    return res.status(200).json(results);
  } catch (err) {
    const error = "Failed to get all events";
    console.error(error);
    console.error(err);
    return res.status(500).json({ error });
  }
});

export interface Signup {
  id?: number;
  signupForm_id: number;
  answer: string;
}

router.get("/signupForm/:id", async (req, res) => {
  const url = `${environment.EVENT_BACKEND}/signupForm/${req.params.id}`;
  try {
    const resp: AxiosResponse<SignupForm> = await axios.get(url);
    return res.status(resp.status).json(resp.data);
  } catch (err) {
    const error = `Failed to get signup form ${req.params.id}`;
    console.error(error);
    console.error(err);
    return res.status(500).json({ error });
  }
});

router.get("/signupForm", async (req, res) => {
  const url = `${environment.EVENT_BACKEND}/signupForm/`;
  try {
    const resp: AxiosResponse<SignupFormResponse> = await axios.get(url);
    const { data: { results } } = resp;
    return res.status(200).json(results);
  } catch (err) {
    const error = "Failed to get all signup forms";
    console.error(error);
    console.error(err);
    return res.status(500).json({ error });
  }
});

router.post("/signup", async (req, res) => {
  const url = `${environment.EVENT_BACKEND}/signup/`;
  // eslint-disable-next-line @typescript-eslint/naming-convention
  const { id, signupForm_id, answer } = req.body;
  try {
    const resp: AxiosResponse<any> = await axios.post(url, {
      id,
      signupForm_id,
      answer,
    });
    const { data: { results } } = resp;
    return res.status(201).json(results);
  } catch (err) {
    const error = "Failed to POST signup";
    console.error(error);
    console.error(err);
    return res.status(err.response.status).json({ error });
  }
});

router.get("/instagram", async (req, res) => {
  const { count } = req.query;
  if (!count) return res.status(400).json({ error: "Missing \"count\" query paramter" });
  const userId = environment.INSTAGRAM_USER_ID;
  const accessToken = environment.INSTAGRAM_ACCESS_TOKEN;
  const fields = [
    "children",
    "caption",
    "media_type",
    "media_url",
    "permalink",
    "timestamp",
    // 'like_count',
  ].join(",");
  const url = `https://graph.instagram.com/${userId}/media?fields=${fields}&access_token=${accessToken}&limit=${count}`;

  // Refresh API token, it lives 60 days
  axios.get(`https://graph.instagram.com/refresh_access_token?grant_type=ig_refresh_token&access_token=${accessToken}`).catch((err) => {
    console.error(err);
  });

  // Actual query
  try {
    const resp: AxiosResponse<InstagramResponse> = await axios.get(url);
    const { data } = resp.data;
    return res.status(200).json(data.map((obj) => ({
      img: obj.media_url,
      link: obj.permalink,
      // tags: obj.tags,
      text: obj.caption,
      id: obj.id,
      // likes: obj.likes.count,
      type: obj.media_type,
    })));
  } catch (err) {
    const error = "Failed to get Instagram feed data.";
    console.error(error);
    console.error(err);
    return res.status(500).json({ error });
  }
});

router.get("/sosso", async (req, res) => {
  const { count } = req.query;
  if (!count) return res.status(400).json({ error: "Missing \"count\" query paramter" });
  const tag = "sik100";
  const apiUrl = `https://sosso.fi/api/get_recent_posts/?tag=${tag}&count=${count}`;

  try {
    const resp: AxiosResponse<SossoResponse> = await axios.get(apiUrl);
    return res.status(200).json(resp.data.posts.map(({
      title, url, excerpt, thumbnail_images,
    }) => ({
      title,
      url,
      excerpt,
      image: thumbnail_images.full.url,
    })));
  } catch (err) {
    const error = "Failed to get Instagram feed data.";
    // eslint-disable-next-line no-console
    console.error(error);
    // eslint-disable-next-line no-console
    console.error(err);
    return res.status(500).json({ error });
  }
});

export default router;
