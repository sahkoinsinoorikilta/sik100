import React from "react";
import styled from "styled-components";
import Button, { ButtonProps } from "./Button";

type ButtonLinkProps = ButtonProps & {
  link: string;
};

const A = styled.a`
  text-decoration: none;

  &:focus,
  &:visited,
  &:link,
  &:active {
    text-decoration: none;
  }
`;

const ButtonLink: React.FC<ButtonLinkProps> = ({ link, ...props }) => (
  <A href={link} target="_blank">
    <Button {...props} />
  </A>
);

export default ButtonLink;
