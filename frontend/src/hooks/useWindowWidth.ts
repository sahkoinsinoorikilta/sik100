import { useState, useEffect } from 'react'

const useWindowWidth = () => {

  const hasWindow = typeof window !== 'undefined';

  const getWindowWidth = () => {
    const width = hasWindow ? window.innerWidth : null;
    return {
      width,
    };
  }

  const [windowWidth, setWindowWidth] = useState(getWindowWidth());

  useEffect(() => {
    const handleResize = () => {
      setWindowWidth(getWindowWidth());
    }
    window.addEventListener('resize', handleResize);
    return () => {
      window.removeEventListener('resize', handleResize);
    }
  }, []);

  return windowWidth
}

export default useWindowWidth;