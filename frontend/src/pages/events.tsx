import React from "react";
import { NextPage, GetStaticProps } from "next";
import { Event } from "../models/events";
import useFetchEvents, { EVENT_URL } from "../hooks/useFetchEvents";
import EventView from "../views/EventView";
import ErrorView from "../views/ErrorView";
import { fetcher } from "../utils";

interface EventInitialProps {
  initialEvents: Event[];
}

const EventsPage: NextPage<EventInitialProps> = ({ initialEvents }) => {
  const { events, error } = useFetchEvents(initialEvents);

  if (error) {
    return (
      <ErrorView statusCode={500} />
    );
  }
  return (
    <EventView events={events ?? []} />
  );
};

export const getStaticProps: GetStaticProps<EventInitialProps> = async () => {
  const initialEvents = await fetcher<Event[]>(EVENT_URL);
  return {
    props: {
      initialEvents,
    },
    revalidate: 60,
  };
};

export default EventsPage;
