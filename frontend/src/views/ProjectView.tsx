import React from "react";
import styled from "styled-components";
import { useTranslation } from "../i18n";
import theme from "../theme";
import Header from "../components/Header";
import Footer from "../components/Footer";
import ButtonLink from "../components/ButtonLink";

const Heading = styled.h2`
  text-align: center;
  text-transform: uppercase;
  color: ${theme.colors.white};
  margin: ${theme.spacing.md} 0;
  font-size: ${theme.fontSizes.h2};
`;

const Container = styled.div`
  display: flex;
  overflow: auto;

  @media screen and (max-width: ${theme.breakpoints.md}) {
    flex-direction: column;
  }
`;

const Info = styled.div`
  flex: 2 0;
  margin: ${(p) => `0 ${p.theme.spacing.md}`};

  p {
    word-break: break-word;
  }

  a {
    text-transform: uppercase;
    text-decoration: none;
    font-weight: bold;
    color: ${theme.colors.white};
    letter-spacing: 1px;

    &:focus,
    &:hover,
    &:visited,
    &:link,
    &:active {
      text-decoration: none;
    }

    &:hover {
      color: ${theme.colors.primaryGold};
      transition: color 0.5s ease-out;
    }
  }

  li {
    width: fit-content;
  }

  @media screen and (max-width: ${theme.breakpoints.sm}) {
    font-size: ${theme.fontSizes.small};
  }
`;

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const ContactContainer = styled.div`
  flex: 1 0;
`;

const ButtonContainer = styled.div`
  width: fit-content;
  align-self: center;
  margin-bottom: ${theme.spacing.md};
`;

const ProjectView: React.FC = () => {
  const { t } = useTranslation("project");
  return (
    <>
      <Header title={t("projectpage")} />
      <main>
        <section>
          <Heading>
            {t("Haluatko olla osa SIK100-vuotta?")}
          </Heading>
          <Container>
            <Info>
              <p>
                {t("Killan jäsenet voivat halutessaan järjestää oman SIK100-tapahtuman tai -projektin")}
                .
              </p>
              <p>
                {t("p1")}
              </p>
              <p>
                {t("Tapahtuman rahoitus on tarkoitus hankkia itse, mutta tarvittaessa SIK100-toimikunta myöntää rahoitusta projektirahastostaan")}
                .
              </p>
              <p>
                {t("Tapahtumasi tai projektisi voi olla melkein mitä vain maan ja taivaan väliltä, kunhan se on SIK100-teemainen ja toteuttaa SIK100-Arvoja")}
                :
              </p>
              <ul>
                <li>
                  {t("Ammattiylpeys")}
                </li>
                <li>
                  {t("Kiltahenki ja yhteisöllisyys")}
                </li>
                <li>
                  {t("SIKin yhteiskunnallisen tärkeyden korostaminen")}
                </li>
                <li>
                  {t("SIKin historian korostus ja nykypäivän innovatiivisuus")}
                </li>
              </ul>
              {/*
              <p>
                {t("projektisuunnitelma")}
                .
                &nbsp;TG: @aatto94
              </p>
              */}

              <ButtonContainer>
                <ButtonLink link="https://docs.google.com/forms/d/e/1FAIpQLSfS_v7xgansiFSzvCKzOyIrwq24Frd5mzZWTti2lbc_vexd_w/viewform?usp=sf_link">
                  {t("Ilmoita ideasi tästä")}
                </ButtonLink>
              </ButtonContainer>

            </Info>
            {/*
            <ContactContainer>
              <ContactImage
                name="Elias Hirvonen"
                image="/images/elias.jpg"
                roleName="SIK100-projektitirehtööri"
                email="elias@sik100.fi"
              />
            </ContactContainer>
            */}
          </Container>
        </section>
      </main>
      <Footer />
    </>
  );
};

export default ProjectView;
