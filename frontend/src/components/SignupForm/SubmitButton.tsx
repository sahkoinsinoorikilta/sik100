import styled from "styled-components";
import theme from "../../theme";

const SubmitButton = styled.button`
  background: ${theme.colors.goldGradient};
  margin: auto;
  cursor: pointer;
  padding: ${(p) => `${p.theme.spacing.sm} ${p.theme.spacing.md}`};
  text-transform: uppercase;
  font-size: ${theme.fontSizes.h2};
  font-weight: bold;
  color: ${theme.colors.darkBlue2};

  @media screen and (max-width: ${theme.breakpoints.sm}) {
    font-size: ${theme.fontSizes.normal};
    padding: ${(p) => `${p.theme.spacing.xs} ${p.theme.spacing.sm}`};
  }
`;

export default SubmitButton;
