import useSWR from "swr";
import config from "../config";
import { InstagramObject } from "../models/instagram";
import { fetcher } from "../utils";

export const INSTAGRAM_URL = `${config.backendUrl}/instagram?count=${config.instagramImageCount}`;

const useFetchInstagram = (initialData: InstagramObject[]) => {
  const { data, error } = useSWR<InstagramObject[]>(INSTAGRAM_URL, fetcher, { initialData });
  return {
    instagramFeed: data,
    error,
  };
};

export default useFetchInstagram;
