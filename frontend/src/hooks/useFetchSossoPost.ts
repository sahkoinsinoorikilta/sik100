import useSWR from "swr";
import config from "../config";
import { SossoPost } from "../models/sosso";
import { fetcher } from "../utils";

const POST_COUNT = 1;
export const SOSSO_URL = `${config.backendUrl}/sosso?count=${POST_COUNT}`;

const useFetchSossoPost = (initialData: SossoPost[]) => {
  const { data, error } = useSWR<SossoPost[]>(SOSSO_URL, fetcher, { initialData });
  return {
    sossoPost: data,
    error,
  };
};

export default useFetchSossoPost;
