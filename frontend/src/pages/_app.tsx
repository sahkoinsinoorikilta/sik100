import React from "react";
import { AppProps } from "next/app";
import Head from "next/head";
import styled, { createGlobalStyle, ThemeProvider } from "styled-components";
import { ToastContainer } from "react-toastify";
import { appWithTranslation } from "../i18n";
import theme from "../theme";

import "react-toastify/dist/ReactToastify.css";

const GlobalCommonStyles = createGlobalStyle`
  body {
    margin: 0;
    line-height: 1.5;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
  }

  * {
    font-family: Montserrat, sans-serif;
    box-sizing: border-box;
  }

  main {
    display: flex;
    flex-direction: column;
    align-items: center;
    flex: 1 0 auto;
    color: ${theme.colors.white};

    section {
      margin: 0 ${theme.spacing.lg};
      max-width: ${theme.maxContentWidth};

      @media screen and (max-width: ${theme.breakpoints.xxxs}) {
        margin: 0 ${theme.spacing.md};
      }
    }
  }
`;

const AppContainer = styled.div`
  display: flex;
  flex-direction: column;
  flex: 1 0 auto;
  align-items: stretch;
  min-height: 100vh;
  background-color: ${theme.colors.darkBlue2};
`;

const SIK100App = ({ Component, pageProps }: AppProps) => (
  <>
    <Head>
      <title>SIK100</title>
    </Head>
    <GlobalCommonStyles />
    <ThemeProvider theme={theme}>
      <AppContainer>
        {/* <Countdown
          revealDate={config.appRevealDate ? new Date(config.appRevealDate) : new Date()}
        > */}
        <Component {...pageProps} />
        {/* </Countdown> */}
        <ToastContainer />
      </AppContainer>
    </ThemeProvider>
  </>
);

export default appWithTranslation(SIK100App);
