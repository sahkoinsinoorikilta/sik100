import axios from "axios";

export const isBrowser = () => typeof window !== "undefined";

export async function fetcher<T>(url: string) {
  const resp = await axios.get<T>(url);
  return resp.data;
}
