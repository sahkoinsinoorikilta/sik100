import React from "react";
import styled from "styled-components";
import Image from "next/image";
import { useTranslation } from "../i18n";
import theme from "../theme";

import Footer from "../components/Footer";
import ButtonLink from "../components/ButtonLink";
import Header from "../components/Header";

const CategoryHeader = styled.h2`
  text-align: center;
  text-transform: uppercase;
  color: ${theme.colors.white};
  margin: ${theme.spacing.md} 0;
  font-size: ${theme.fontSizes.h2};
`;

const ClothesContainer = styled.div`
  display: flex;
  justify-content: center;
  margin-bottom: ${theme.spacing.xlg};

  @media screen and (max-width: ${theme.breakpoints.sm}) {
    flex-direction: column;
    align-items: center;
  }
`;

const ProductName = styled.div`
  font-size: ${theme.fontSizes.h2};
  color: ${theme.colors.secondaryGold};
  text-transform: uppercase;

  @media screen and (max-width: ${theme.breakpoints.sm}) {
    font-size: ${theme.fontSizes.normal};
  }
`;

const ProductPrice = styled.div`
  font-size: ${theme.fontSizes.h2};
  color: ${theme.colors.white};
  text-transform: uppercase;

  @media screen and (max-width: ${theme.breakpoints.sm}) {
    font-size: ${theme.fontSizes.normal};
  }
`;

// const BooksContainer = styled.div``;

type ClothesProductProps = {
  name: string;
  image: string;
  price: string;
  description: string;
  link: string;
};
const ClothesProduct: React.FC<ClothesProductProps> = ({
  name, image, price, description, link,
}) => {
  const { t } = useTranslation("products");
  const Container = styled.div`
    display: flex;
    flex-direction: column;
    margin: 0 ${theme.spacing.lg};
    max-width: 400px;

    @media screen and (max-width: ${theme.breakpoints.md}) {
      margin: 0 ${theme.spacing.md};
    }
`;

  const Info = styled.div`
    display: flex;
    flex-direction: column;
    margin: ${theme.spacing.sm};

    p {
      @media screen and (max-width: ${theme.breakpoints.sm}) {
        font-size: ${theme.fontSizes.small};
      }
    }
`;
  const NameAndPrice = styled.div`
    display: flex;
    justify-content: space-between;
`;
  const ButtonContainer = styled.div`
    width: fit-content;
    align-self: center;
`;

  return (
    <Container>
      <Image
        src={image}
        alt={name}
        width={300}
        height={300}
        layout="responsive"
        loading="lazy"
      />
      <Info>
        <NameAndPrice>
          <ProductName>
            {t(name)}
          </ProductName>
          <ProductPrice>{`${price}€`}</ProductPrice>
        </NameAndPrice>
        <p>
          {t(description)}
        </p>
        <ButtonContainer>
          <ButtonLink link={link}>
            {t("Tilaa")}
          </ButtonLink>
        </ButtonContainer>
      </Info>
    </Container>
  );
};

// type BookProductProps = {
//   name: string;
//   price: string;
//   description: string;
//   image?: string;
// }

// const BookProduct: React.FC<BookProductProps> = ({ name, price, description }) => {
//   const Container = styled.div``;
//   const Image = styled.img``;
//   const Info = styled.div``;
//   return (
//     <Container>
//       <Image />
//       <Info>
//         <ProductName>{name}</ProductName>
//         <ProductPrice>{`${price}€`}</ProductPrice>
//         {description}
//       </Info>
//     </Container>
//   );
// };

const ProductsView: React.FC = () => {
  const { t } = useTranslation("products");
  return (
    <>
      <Header title={t("productpage")} golden />
      <main>
        <section>
          <CategoryHeader>{t("Vaatteet")}</CategoryHeader>
          <ClothesContainer>
            {/*
            <ClothesProduct
              name="Paita"
              price="45"
          // eslint-disable-next-line max-len
              description="Tyylikkäät vaatteet kruunaavat juhlavuoden! Tilaa Pure Wasten 100% kier
              rätysmateriaaleista valmistettu, näyttävä SIK100-collegepaita
              ja anna rakkautesi Aalto-yliopiston Sähköinsinöörikiltaa kohtaan näkyä!"
              image="/images/paita.jpg"
              link="https://forms.gle/KP2mGXj91kg1P6jf6"
            />
            */}
            <ClothesProduct
              name="Pipo"
              price="20"
          // eslint-disable-next-line max-len
              description="Haluatko pitää pään lämpimänä, näyttää hyvältä ja näytää rakkautesi SIKin 100-vuotista historiaa ja vielä pidempää tulevaisuutta kohti? Tilaa Pure Wasten 100% kierrätysmateriaaleista tehty, tyylikäs SIK100-pipo!"
              image="/images/pipo.jpg"
              link="https://forms.gle/KP2mGXj91kg1P6jf6"
            />
          </ClothesContainer>
          {/* <BooksContainer>
        <BookProduct
          name="Historiikki"
          price={""}
          description="SIK:n 100-vuotishistoriikki julkaistaan " />
        <BookProduct
          name="Juotos"
          price={"20"}
          description="Lorem" />
      </BooksContainer> */}
        </section>
      </main>
      <Footer />
    </>
  );
};

export default ProductsView;
