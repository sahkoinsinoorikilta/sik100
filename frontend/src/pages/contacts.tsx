import React from "react";
import { NextPage, GetStaticProps } from "next";
import ContactsView from "../views/ContactView";

const ContactsPage: NextPage = () => (
  <ContactsView />
);

export const getStaticProps: GetStaticProps = async () => ({
  props: {},
});

export default ContactsPage;
