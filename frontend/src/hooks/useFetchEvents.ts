import useSWR from "swr";
import config from "../config";
import { Event } from "../models/events";
import { fetcher } from "../utils";

export const EVENT_URL = `${config.backendUrl}/events`;

const useFetchEvents = (initialData?: Event[]) => {
  const { data, error } = useSWR<Event[] | undefined>(EVENT_URL, fetcher, { initialData, revalidateOnFocus: false });
  return {
    events: data,
    error,
  };
};

export default useFetchEvents;
