import React from "react";
import styled from "styled-components";
import Navbar from "../components/Navbar";

const Container = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  flex: 1 0 auto;
  color: ${(p) => p.theme.colors.white};

  h1,
  h3 {
    margin: 0;
  }
`;

interface ErrorViewProps {
  statusCode: number;
}

const ErrorView: React.FC<ErrorViewProps> = ({ statusCode, children }) => (
  <>
    <Navbar />
    <Container>
      <div>
        <h1>{statusCode}</h1>
        {children}
      </div>
    </Container>
  </>
);

export default ErrorView;
