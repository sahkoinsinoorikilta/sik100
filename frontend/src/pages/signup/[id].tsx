import React from "react";
import { NextPage, GetStaticProps, GetStaticPaths } from "next";
import { useRouter } from "next/router";
import { ParsedUrlQuery } from "querystring";
import SignupView from "../../views/SignupView";
import useFetchSignup, { SIGNUP_FORM_URL } from "../../hooks/useFetchSignup";
import ErrorView from "../../views/ErrorView";
import { fetcher } from "../../utils";
import { EventSignupForm } from "../../models/events";

interface SignupInitialProps {
  initialForm?: EventSignupForm;
}

interface IParams extends ParsedUrlQuery {
  id: string
}

const SignUpPage: NextPage<SignupInitialProps> = ({ initialForm }) => {
  const router = useRouter();
  const id = String(initialForm ? initialForm.id : "");
  const { signupForm, error } = useFetchSignup(initialForm, id);
  if (router.isFallback) {
    return (
      <ErrorView statusCode={404} />
    );
  }

  if (error) {
    return (
      <ErrorView statusCode={500} />
    );
  }
  if (!signupForm) {
    return (
      <ErrorView statusCode={404} />
    );
  }
  return (
    <SignupView signupForm={signupForm} />
  );
};

export const getStaticPaths: GetStaticPaths = async () => {
  const allForms = await fetcher<EventSignupForm[]>(SIGNUP_FORM_URL);
  const paths = allForms.map((e: EventSignupForm) => ({
    params: {
      id: String(e.id),
    },
  }
  ));
  return {
    paths,
    fallback: true,
  };
};

export const getStaticProps: GetStaticProps = async ({ params }) => {
  const { id } = params as IParams;
  let initialForm: EventSignupForm | undefined;
  let notFound = false;

  try {
    initialForm = await fetcher<EventSignupForm>(`${SIGNUP_FORM_URL}${id}/`);
  } catch (e) {
    notFound = true;
  }

  return {
    props: {
      initialForm,
    },
    revalidate: 10, // Required for deleting hidden pages
    notFound,
  };
};

export default SignUpPage;
