import React, { useState, useEffect, useRef } from "react";
import ReactMarkdown from "react-markdown";
import rehypeRaw from "rehype-raw";
import rehypeSanitize from "rehype-sanitize";
import styled from "styled-components";
import Image from "next/image";
import noop from "lodash/noop";
import NextJSLink from "next/link";
import { useTranslation } from "../../i18n";
import theme from "../../theme";
import Button from "../Button";
import { EventSignupForm } from "../../models/events";
import { isBrowser } from "../../utils";

type EventCardProps = {
  title: string;
  start: string;
  location: string;
  content: string;
  signupForms?: EventSignupForm[];
  focused?: boolean;
  image?: string;
};

const EventContainer = styled.article`
  display: flex;
  flex-direction: column;
  background-color: ${theme.colors.blue3};
  margin: ${theme.spacing.sm} 0;

  & > div:first-of-type {
    display: flex;
    flex-flow: row wrap;
    cursor: pointer;
  }
`;

const Date = styled.p`
  margin: 0;
  min-width: 90px;
  flex: 1 0;
  background: ${theme.colors.goldGradient};
  color: ${theme.colors.darkBlue1};
  display: flex;
  justify-content: center;
  align-items: center;
  font-size: ${theme.fontSizes.biggest};
  font-weight: bold;
  letter-spacing: 2px;

  @media screen and (max-width: ${theme.breakpoints.sm}) {
    font-size: ${theme.fontSizes.bigger};
  }

  @media screen and (max-width: ${theme.breakpoints.xxs}) {
    font-size: ${theme.fontSizes.h2};
  }

  @media screen and (max-width: ${theme.breakpoints.xxxs}) {
    font-size: ${theme.fontSizes.normal};
  }
`;

const TextContainer = styled.div`
  flex: 4 0;
  white-space: nowrap;
  text-overflow: ellipsis;
  overflow: hidden;
  margin: ${(p) => `${p.theme.spacing.sm} ${p.theme.spacing.md}`};

  & > p {
    white-space: nowrap;
    text-overflow: ellipsis;
    overflow: hidden;
    margin: ${theme.spacing.xxs} 0;
    color: ${theme.colors.white};

    @media screen and (max-width: ${theme.breakpoints.xxxs}) {
      font-size: ${theme.fontSizes.small};
    }
  }

  @media screen and (max-width: ${theme.breakpoints.sm}) {
    flex: 3 0;
  }

  @media screen and (max-width: ${theme.breakpoints.xs}) {
    margin-right: ${theme.spacing.xxs};
  }
`;

const Name = styled.p`
  color: ${theme.colors.turquoise} !important;
  font-size: ${theme.fontSizes.bigger};
  text-transform: uppercase;
  font-weight: 700;
  letter-spacing: 1px;

  @media screen and (max-width: ${theme.breakpoints.sm}) {
    font-size: ${theme.fontSizes.h2};
  }

  @media screen and (max-width: ${theme.breakpoints.xxs}) {
    font-size: ${theme.fontSizes.normal};
  }
`;

const OpenableContent = styled.div`
  color: ${theme.colors.white};
  background-color: ${theme.colors.blue6};
  padding: ${(p) => `${p.theme.spacing.lg} ${p.theme.spacing.xlg}`};

  @media screen and (max-width: ${theme.breakpoints.md}) {
    padding: ${(p) => `${p.theme.spacing.lg}`};
  }

  @media screen and (max-width: ${theme.breakpoints.sm}) {
    padding: ${(p) => `${p.theme.spacing.md}`};
  }

  @media screen and (max-width: ${theme.breakpoints.xxs}) {
    padding: ${(p) => `${p.theme.spacing.sm} ${p.theme.spacing.md}`};
  }

  & > p {
    margin: 12px 0;
  }

  & > form {
    margin-top: ${theme.spacing.sm};
  }

  & > h3 {
    color: ${theme.colors.secondaryGold};
  }

  a {
    font-weight: bold;
    color: ${theme.colors.secondaryGold};
    letter-spacing: 1px;
    text-decoration: underline;

    &:hover {
      color: ${theme.colors.primaryGold};
    }
  }

  & > table {
    tr {
      vertical-align: top;

      td {
        padding: ${theme.spacing.xxs} ${theme.spacing.xs};
        word-break: break-word;
      }

      td:first-of-type {
        word-break: unset;
      }
    }
  }

  button {
    margin-top: ${theme.spacing.sm};
  }
`;

const ExpandableIcon = styled.div`
  font-size: 32px;
  height: 48px;
  width: 48px;
  text-align: center;
  justify-self: center;
  align-self: center;
  margin-right: 8px;
  color: ${theme.colors.white};

  @media screen and (max-width: ${theme.breakpoints.sm}) {
    height: 32px;
    width: 32px;
    font-size: 24px;
  }
`;

const SignupButtons = styled.div`
  display: flex;
  flex-flow: row wrap;
  justify-content: space-around;

  & > button {
    & > div {
      font-weight: bold;
    }
  }
`;

const EventCard: React.FC<EventCardProps> = ({
  title, start, location, content, signupForms, focused, image,
}) => {
  const [isOpen, setOpen] = useState(focused);
  const htmlElRef = useRef<HTMLElement>(null);
  const { t, i18n } = useTranslation();

  useEffect(() => {
    if (focused && isBrowser()) {
      window.scrollTo(0, htmlElRef.current?.offsetTop ?? 0);
    }
  }, [focused]);

  const eventInfo = (
    <>
      {image && (
      <Image
        src={image}
        alt={title}
        layout="responsive"
        width={200}
        height={100}
        objectFit="scale-down"
        loading="lazy"
      />
      )}
      <ReactMarkdown rehypePlugins={[rehypeRaw, rehypeSanitize]}>
        {content}
      </ReactMarkdown>
      <SignupButtons>
        {signupForms && signupForms.map((form) => (
          <NextJSLink key={form.id} href={`/signup/${form.id}/`}>
            <Button
              onClick={() => noop()}
            >
              {
                i18n.language === "en" ? form.title_en : form.title_fi
              }
            </Button>
          </NextJSLink>
        ))}
      </SignupButtons>
    </>
  );

  return (
    <EventContainer ref={htmlElRef}>
      {/* eslint-disable jsx-a11y/click-events-have-key-events */}
      {/* eslint-disable jsx-a11y/no-static-element-interactions */}
      <div onClick={() => setOpen(!isOpen)}>
        <Date>{start}</Date>
        <TextContainer>
          <Name>{title}</Name>
          <p>
            {`@${location}`}
          </p>
        </TextContainer>
        <ExpandableIcon>
          {isOpen ? "-" : "+"}
        </ExpandableIcon>
      </div>
      {isOpen && (
        <OpenableContent>
          {eventInfo}
        </OpenableContent>
      )}
    </EventContainer>
  );
};

export default EventCard;
