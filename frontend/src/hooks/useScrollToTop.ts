import { useEffect } from "react";

export const scrollToTop = () => {
  window.scroll({
    top: 0,
    left: 0,
    behavior: "smooth",
  });
};

const useScrollToTop = () => {
  useEffect(() => {
    if (window) {
      scrollToTop();
    }
  }, []);
};

export default useScrollToTop;
