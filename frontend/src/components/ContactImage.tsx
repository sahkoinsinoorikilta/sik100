import React from "react";
import Image from "next/image";
import styled from "styled-components";
import { useTranslation } from "../i18n";
import theme from "../theme";

const Contact = styled.figure`
  margin: ${theme.spacing.lg};
  display: flex;
  flex-direction: column;
  align-items: center;

  img {
    border-radius: 100px;
  }

  figcaption {
    color: ${theme.colors.white};
    text-align: left;
    padding: ${theme.spacing.sm} 0;
    display: inline-block;
    max-width: 200px;
    width: 100%;

    a {
      color: ${theme.colors.white};
    }
  }

  @media screen and (max-width: ${theme.breakpoints.xxxs}) {
    margin: ${theme.spacing.sm};
  }
`;

type ContactImageProps = {
  name: string;
  image: string;
  roleName: string;
  email: string;
};

const ContactImage: React.FC<ContactImageProps> = ({
  name, image, roleName, email,
}) => {
  const { t } = useTranslation();
  return (
    <Contact key={name}>
      <Image
        src={image}
        alt={name}
        width={200}
        height={200}
        objectFit="cover"
        loading="lazy"
      />
      <figcaption>
        <strong>
          {name}
        </strong>
        <br />
        {t(roleName)}
        <br />
        <a href={`mailto:${email}`}>
          {email}
        </a>
      </figcaption>
    </Contact>
  );
};

export default ContactImage;
