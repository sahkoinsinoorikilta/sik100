import React from "react";
import { NextPage, GetStaticProps } from "next";
import { Event } from "../models/events";
import { InstagramObject } from "../models/instagram";
import { SossoPost } from "../models/sosso";
import useFetchEvents, { EVENT_URL } from "../hooks/useFetchEvents";
import useFetchInstagram, { INSTAGRAM_URL } from "../hooks/useFetchInstagram";
import useFetchSossoPost, { SOSSO_URL } from "../hooks/useFetchSossoPost";
import LandingView from "../views/LandingView";
import { fetcher } from "../utils";

interface LandingInitialProps {
  initialEvents: Event[];
  initialInstagramFeed: InstagramObject[];
  initialSossoPost: SossoPost;
}

const LandingPage: NextPage<LandingInitialProps> = ({
  initialEvents,
  initialInstagramFeed,
  initialSossoPost,
}) => {
  const { events } = useFetchEvents(initialEvents);
  const { instagramFeed } = useFetchInstagram(initialInstagramFeed);
  const { sossoPost } = useFetchSossoPost([initialSossoPost]);
  return (
    <LandingView
      events={events ?? []}
      instagramFeed={instagramFeed ?? []}
      sossoPost={sossoPost?.[0]}
    />
  );
};

export const getStaticProps: GetStaticProps<LandingInitialProps> = async () => {
  const initialEvents = fetcher<Event[]>(EVENT_URL);
  const initialInstagramFeed = fetcher<InstagramObject[]>(INSTAGRAM_URL);
  const initialSossoPost = fetcher<SossoPost[]>(SOSSO_URL);
  return {
    props: {
      initialEvents: await initialEvents,
      initialInstagramFeed: await initialInstagramFeed,
      initialSossoPost: (await initialSossoPost)?.[0],
    },
    revalidate: 60,
  };
};

export default LandingPage;
