export type SossoPost = {
  title: string;
  url: string
  excerpt: string;
  image: string;
};
