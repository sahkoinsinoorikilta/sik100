import styled from "styled-components";
import theme from "../theme";

const GoldHR = styled.hr`
  margin-block-start: 0;
  margin-block-end: 0;
  width: 100%;
  border: 0;
  background: ${theme.colors.goldGradient};
  height: 8px;
`;

export default GoldHR;
