/* eslint-disable camelcase */

export type EventTag = {
  id: number;
  slug: string;
  name: string;
  name_fi: string;
  name_en: string;
  icon: string;
};

type OptionTypes =
  "text" |
  "info" |
  "integer" |
  "radiobutton" |
  "checkbox" |
  "email" |
  "name";

type SchemaTypes = 
  "string" |
  "number" |
  "integer" |
  "array" |
  "boolean" |
  "null";

export interface SignupFormQuestion {
  id: string;
  title_fi: string;
  title_en: string;
  description_fi?: string;
  description_en?: string;
  type: OptionTypes;
  options: {
    enum: string[];
    enumNames_fi: string[];
    enumNames_en: string[];
  };
  required?: boolean;
}

export interface EventSignupForm {
  id: number;
  title_fi: string;
  title_en: string;
  visible: boolean;
  isOpen: boolean;
  start_time: string;
  end_time: string;
  email_content: string;
  questions: SignupFormQuestion[];
  signups: string[];
  quota: number;
  schema: {
    title?: string;
    type: SchemaTypes;
    required: string[];
    properties: any;
    minProperties?: number;
  };
}

export type Event = {
  id: number;
  tag_id: number[];
  tags: EventTag[];
  visible: boolean;
  title_fi: string;
  title_en: string;
  description_fi: string;
  description_en: string;
  content_fi: string;
  content_en: string;
  start_time: string;
  end_time: string;
  location_fi: string;
  location_en: string;
  signup_id: number[];
  signupForm: EventSignupForm[];
  image?: string;
};
