import React, { useState } from "react";
import Form, { ISubmitEvent } from "react-jsonschema-form";
import styled from "styled-components";
import { useTranslation } from "../../i18n";
import theme from "../../theme";
import { EventSignupForm, SignupFormQuestion } from "../../models/events";
import Checkboxes from "./Checkboxes";
import RadioButtonWidget from "./RadioButtonWidget";
import SubmitButton from "./SubmitButton";
import { onChangeFunc } from "./SeminarOverrides";
import GDPRCheckbox from "./GDPRCheckbox";
import config from "../../config";

interface SignupFormProps {
  signup: EventSignupForm;
  onSubmit: (e: ISubmitEvent<any>) => any;
}

const customWidgets = {
  radio: RadioButtonWidget,
  checkboxes: Checkboxes,
};

const questionToUISchema = (question: SignupFormQuestion) => {
  let obj = {};
  if (question.type === "checkbox") {
    obj = {
      "ui:widget": "checkboxes",
    };
  } else if (question.type === "radiobutton") {
    obj = {
      "ui:widget": "radio",
    };
  }
  return {
    [question.id]: obj,
  };
};

const buildUISchema = (questions: SignupFormQuestion[]) => {
  const uiSchemaPropsArray = questions.map(questionToUISchema);
  let uiSchemaProps = {};
  uiSchemaPropsArray.forEach((uiSchemaProp) => {
    uiSchemaProps = {
      ...uiSchemaProps,
      ...uiSchemaProp,
    };
  });

  const uiSchema = {
    ...uiSchemaProps,
  };
  return uiSchema;
};

const StyledForm = styled(Form)`
  font-size: ${theme.fontSizes.normal};

  @media screen and (max-width: ${theme.breakpoints.sm}) {
    font-size: ${theme.fontSizes.small};
  }

  @media screen and (max-width: ${theme.breakpoints.xxs}) {
    font-size: ${theme.fontSizes.smaller};
  }

  fieldset {
    border: none;
    padding: 0;
    margin: 1em 0;
    display: block;
    margin-inline-start: 0;
    margin-inline-end: 0;
    padding-inline-start: 0;
    padding-inline-end: 0;

    .form-group {
      padding-bottom: 0.5em;
      display: flex;
      flex-direction: column;

      & > label {
        margin: 0.5em 0;
        font-weight: 600;
        display: block;
      }
    }

    input[type="text"],
    input[type="email"],
    input[type="number"] {
      display: block;
      height: 48px;
      flex: 1 0 auto;
      font-size: 1.5em;
      padding: 0 16px;
    }
  }

  & > div:last-child {
    text-align: center;
  }
`;

const Header = styled.h2`
  font-size: ${theme.fontSizes.h2};

  @media screen and (max-width: ${theme.breakpoints.sm}) {
    font-size: ${theme.fontSizes.normal};
  }
`;

const TotalPrice = styled.div`
  font-size: ${theme.fontSizes.normal};
  margin-top: 1em;

  @media screen and (max-width: ${theme.breakpoints.sm}) {
    font-size: ${theme.fontSizes.small};
  }

  @media screen and (max-width: ${theme.breakpoints.xxs}) {
    font-size: ${theme.fontSizes.smaller};
  }
`;

const SignupForm: React.FC<SignupFormProps> = ({ signup, onSubmit }) => {
  const { t, i18n } = useTranslation();
  const startDate = new Date(signup?.start_time);
  const endDate = new Date(signup?.end_time);
  const dateStringFormatOptions: Intl.DateTimeFormatOptions = {
    day: "2-digit", month: "2-digit", year: "numeric", hour: "2-digit", minute: "2-digit",
  };
  const properties: Record<string, any> = {};
  signup.questions.forEach(({
    id, options, title_fi, title_en, description_fi, description_en,
  }: SignupFormQuestion) => {
    properties[id] = {
      ...signup.schema.properties[id],
      title: i18n.language === "en" ? title_en : title_fi,
      description: i18n.language === "en" ? description_en : description_fi,
      enumNames: i18n.language === "en" ? options.enumNames_en : options.enumNames_fi,
    };
  });
  const formSchema = {
    ...signup.schema,
    properties,
  };

  const uiSchema = buildUISchema(signup.questions);

  // SIK100 seminar override
  const [controlledForm, setFormData] = useState();
  const [totalPrice, setTotalPrice] = useState(0);
  let formData;
  let onChange;
  if (signup.id === Number(config.seminaariSignupId)) {
    formData = controlledForm;
    onChange = onChangeFunc(setFormData, setTotalPrice);
  }

  if (startDate > new Date()) {
    return (
      <>
        <Header>
          {t("Ilmoittautuminen ei ole vielä auki!")}
        </Header>
        {t("Se aukeaa")}
        &nbsp;
        {new Date(signup.start_time).toLocaleString(i18n.language, dateStringFormatOptions)}
      </>
    );
  } if (new Date() > endDate) {
    return (
      <Header>
        {t("Ilmoittautuminen on umpeutunut!")}
      </Header>
    );
  }

  return (
    <>
      <Header>
        {i18n.language === "en" ? signup.title_en.toUpperCase() : signup.title_fi.toUpperCase()}
      </Header>
      <p>
        {`
        ${
          t("Ilmoittautuminen sulkeutuu")
        }
        ${
        new Date(signup.end_time)
          .toLocaleString(i18n.language, dateStringFormatOptions)}
        `}
        .
      </p>
      <StyledForm
        schema={formSchema}
        uiSchema={uiSchema}
        onSubmit={onSubmit}
        onChange={onChange}
        formData={formData}
        widgets={customWidgets}
        idPrefix="rjsf"
      >
        <div>
          <GDPRCheckbox>
            {t("Ilmoittautumalla hyväksyn")}
            &nbsp;
            <a href="http://sik.ayy.fi/files/official/Tietosuojaseloste%20%E2%80%93%20Tapahtumien%20ilmoittautumisrekisteri.pdf">
              {t("tietosuojaselosteen")}
            </a>
            &nbsp;
            {t("ja tietojeni tallentamisen.")}
          </GDPRCheckbox>
        </div>

        {signup.id === Number(config.seminaariSignupId) && (
          <TotalPrice>
            {t("Kokonaishinta")}
            :&nbsp;
            <strong>
              {`${totalPrice} €`}
            </strong>
          </TotalPrice>
        )}

        <div>
          <SubmitButton>
            {t("Ilmoittaudu")}
          </SubmitButton>
        </div>
      </StyledForm>
    </>
  );
};

export default SignupForm;
