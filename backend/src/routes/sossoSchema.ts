export type SossoResponse = {
  status: string;
  count: number;
  count_total: number;
  pages: number;
  posts: Post[];
};

type Image = {
  url: string;
  width: number;
  height: number;
};

type Post = {
  id: number;
  type: "post";
  slug: string;
  url: string;
  title: string;
  title_plain: string;
  content: string;
  excerpt: string;
  date: Date;
  modified: Date;
  thumbnail: string;
  thumbnail_images: {
    medium: Image;
    full: Image;
  }
};
