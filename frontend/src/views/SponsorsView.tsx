import React from "react";
import styled from "styled-components";

import { useTranslation } from "../i18n";
import theme from "../theme";
import ContactImage from "../components/ContactImage";
import Footer from "../components/Footer";
import Header from "../components/Header";

const Container = styled.div`
  display: flex;
  overflow: auto;
  flex-direction: column;
`;

const Info = styled.div`
  flex: 2;
  margin: ${(p) => `0 ${p.theme.spacing.md}`};

  p {
    word-break: break-word;
  }
`;

const ContactContainer = styled.div`
  flex: 1;
  display: flex;
  flex-direction: row;
  justify-content: center;

  @media screen and (max-width: ${theme.breakpoints.sm}) {
    flex-direction: column;
  }
`;

export const SponsorsPage: React.FC = () => {
  const { t } = useTranslation("sponsors");
  return (
    <>
      <Header title={t("sponsorspage")} golden>
        <p>
          {t("banner")}
        </p>

      </Header>
      <main>
        <section>
          <Container>
            <Info>
              <p>
                {t("p1")}
              </p>
              <p>
                {t("p2")}
              </p>
              <p>
                {t("p3")}
              </p>
            </Info>
            <ContactContainer>
              <ContactImage name="Juuli Leppänen" image="/images/juuli.jpg" roleName="SIK100-yritysvastaava" email="juuli@sik100.fi" />
            </ContactContainer>
          </Container>
        </section>
      </main>
      <Footer />
    </>
  );
};

export default SponsorsPage;
