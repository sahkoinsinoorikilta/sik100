import React from "react";
import Image from "next/image";
import Marquee from "react-marquee-slider";
import noop from "lodash/noop";
import styled from "styled-components";
import theme from "../theme";

const ImgContainer = styled.div`
  margin: 0 ${theme.spacing.lg};
`;

const IgnorePageMargins = styled.div`
  margin: 0;
  max-width: unset;
`;

interface MinorSponsorMarqueeProps {
  speed: number;
  imgSize: number;
  imgs: {
    src: string;
    alt: string;
  }[];
}

const MinorSponsorMarquee: React.FC<MinorSponsorMarqueeProps> = ({ speed, imgSize, imgs }) => (
  <IgnorePageMargins>
    <Marquee velocity={speed} direction="rtl" scatterRandomly={false} resetAfterTries={0} onInit={noop} onFinish={noop}>
      {imgs.map(({ src, alt }) => (
        <ImgContainer key={alt}>
          <Image
            src={src}
            alt={alt}
            width={imgSize}
            height={imgSize}
            objectFit="scale-down"
          />
        </ImgContainer>
      ))}
    </Marquee>
  </IgnorePageMargins>
);

export default MinorSponsorMarquee;
