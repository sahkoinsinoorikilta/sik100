import React from "react";
import { NextPage, GetStaticProps } from "next";
import { Event } from "../models/events";
import useFetchEvents, { EVENT_URL } from "../hooks/useFetchEvents";
import EventView from "../views/EventView";
import ErrorView from "../views/ErrorView";
import config from "../config";
import { fetcher } from "../utils";

interface EventInitialProps {
  initialEvents: Event[];
}

const SeminaariIlmoPage: NextPage<EventInitialProps> = ({ initialEvents }) => {
  const { events, error } = useFetchEvents(initialEvents);
  if (error) {
    return (
      <ErrorView statusCode={500} />
    );
  }

  return (
    <EventView events={events ?? []} focusOn={Number(config.seminaariEventId)} />
  );
};

export const getStaticProps: GetStaticProps<EventInitialProps> = async () => {
  const initialEvents = await fetcher<Event[]>(EVENT_URL);
  return {
    props: {
      initialEvents,
    },
    revalidate: 60,
  };
};

export default SeminaariIlmoPage;
