const config = {
  backendUrl: process.env.NEXT_PUBLIC_BACKEND_HTTP_URL,
  instagramImageCount: process.env.NEXT_PUBLIC_INSTAGRAM_IMAGE_COUNT,
  appRevealDate: process.env.NEXT_PUBLIC_REVEAL_DATE,
  seminaariEventId: process.env.NEXT_PUBLIC_SEMINAARI_EVENT_ID,
  seminaariSignupId: process.env.NEXT_PUBLIC_SEMINAARI_SIGNUP_ID,
};

export default config;
