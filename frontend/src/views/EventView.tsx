import React from "react";
import styled from "styled-components";
import { useTranslation } from "../i18n";
import { Event } from "../models/events";
// import Sponsors from "../../components/Sponsors";
// import GoldHR from "../../components/GoldHR";
import Footer from "../components/Footer";
import UpcomingEvents from "../components/UpcomingEvents";
import Header from "../components/Header";

const InfoSection = styled.section`
  p {
    word-break: break-word;
  }
`;

const EventSection = styled.section`
  width: 100%;
`;

interface EventViewProps {
  focusOn?: number;
  events: Event[];
}

const EventView: React.FC<EventViewProps> = ({ events, focusOn }) => {
  const { t } = useTranslation("events");
  return (
    <>
      <Header title={t("events")} />
      <main>
        <InfoSection>
          <p>
            {t("p1")}
          </p>
          <p>
            {t("Muistahan säännöllisesti kurkata tätä kalenteria ettet missaa yhtään ikimuistoista tapahtumaa!")}
          </p>
        </InfoSection>
        <EventSection>
          <UpcomingEvents events={events} focusOn={focusOn} />
        </EventSection>
        {/* <section>
          <Sponsors />
        </section> */}
        {/* <GoldHR /> */}
      </main>
      <Footer />
    </>
  );
};

export default EventView;
