const withPWA = require("next-pwa");
const withBundleAnalyzer = require("@next/bundle-analyzer")({
  enabled: process.env.ANALYZE === "true",
});

const localeSubpaths = {};

const settings = {
  target: "serverless",
  publicRuntimeConfig: {
    localeSubpaths,
  },
  pwa: {
    disable: process.env.NODE_ENV === "development",
    register: true,
    dest: "public",
  },
  images: {
    domains: [
      "sahkoinsinoorikilta.fi",
      "api.sahkoinsinoorikilta.fi",
      "static.sahkoinsinoorikilta.fi",
      "dev.sahkoinsinoorikilta.fi",
      "api.dev.sahkoinsinoorikilta.fi",
      "sosso.fi",
    ],
  },
};

module.exports = withBundleAnalyzer(withPWA(settings));
