import React from "react";
import styled from "styled-components";
import { useTranslation } from "../i18n";
import { InstagramObject } from "../models/instagram";
import theme from "../theme";

const Instagram = styled.div`
  padding: 0 ${theme.spacing.md};

  h2 {
    color: ${theme.colors.yellow};
    text-transform: uppercase;
    text-align: center;
    margin-top: ${theme.spacing.lg};
  }

  & > div {
    display: flex;
    flex-direction: row;
    flex-wrap: wrap;
    justify-content: space-around;
  }

  figure {
    color: ${theme.colors.white};
    margin: ${theme.spacing.sm};
    width: 250px;

    img,
    video {
      width: 100%;
      height: 100%;
      object-fit: scale-down;
    }

    @media screen and (max-width: ${theme.breakpoints.xxxs}) {
      margin: 0;
      width: 200px;
    }
  }
`;

const ImageBackground = styled.div`
  width: 100%;
  height: 250px;

  @media screen and (max-width: ${theme.breakpoints.xxxs}) {
    height: 200px;
  }

  background-color: black;
  display: flex;
  justify-content: center;
  align-items: center;
`;

const ImgCaption = styled.figcaption`
  color: ${theme.colors.white};
  font-size: 14px;
  text-decoration: none;
  overflow: hidden;
  margin: 1em 0;

  /*
  custom three dots for multiline component
  hide text if it more than N lines
  use this value to count block height
  */

  position: relative;

  /* max-height = line-height (1.5) * lines max number (3) */
  line-height: 1.5;
  max-height: 4.5em;
  transition: max-height 0.5s cubic-bezier(0, 1, 0, 1);

  /* fix problem when last visible word doesn't adjoin right side  */
  text-align: justify;

  /* place for "..." */
  margin-right: -1em;
  padding-right: 1em;

  /* create the ... */
  &::before {
    content: "...";
    position: absolute;
    right: 0;
    bottom: 0;
  }

  /* hide ... if we have text, which is less than or equal to max lines */
  &::after {
    content: "";
    position: absolute;
    right: 0;
    width: 1em;
    height: 1em;
    margin-top: 0.2em;
    background: ${theme.colors.blue6};
  }

  &:hover {
    /* Just something bigger than the text will ever be. Transition does not work for fit-content or anything not exactly a number */
    max-height: 2000px;
    transition: max-height 1s ease-in-out;

    /* hide the ... */
    &::before {
      content: "";
    }

    &::after {
      background: transparent;
    }
  }
`;

interface InstagramFeedProps {
  feed: InstagramObject[];
}

const InstagramFeed: React.FC<InstagramFeedProps> = ({ feed }) => {
  const { t } = useTranslation();
  return (
    <Instagram>
      <h2>{t("Seuraa sosiaalisessa mediassa")}</h2>
      <div>
        {feed.map((instagramObject) => (
          <figure key={instagramObject.id}>
            <a
              href={instagramObject.link}
              aria-label="Instagram"
            >
              <ImageBackground>
                {instagramObject.type === "VIDEO"
                  ? (
                // eslint-disable-next-line jsx-a11y/media-has-caption
                    <video controls>
                      <source src={instagramObject.img} type="video/mp4" />
                    </video>
                  )
                  : (
                    // eslint-disable-next-line @next/next/no-img-element
                    <img
                      src={instagramObject.img}
                      alt={`Instagram-${instagramObject.id}`}
                      loading="lazy"
                    />
                  )}
              </ImageBackground>
            </a>
            <ImgCaption>{instagramObject.text}</ImgCaption>
          </figure>
        ))}
      </div>
    </Instagram>
  );
};

export default InstagramFeed;
